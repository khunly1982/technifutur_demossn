using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DemoUnitTest.Test
{
    [TestClass]
    public class PersonneTest
    {
        [TestMethod]
        public void ValidateSSNWithBadFormat()
        {
            Personne p = new Personne();

            p.DateDeNaissance = new System.DateTime(1982,5,6);
            p.Genre = 2;
            p.SSN = "820506-";

            bool result = p.ValidateSSN();

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateSSNWithBadGender()
        {
            Personne p = new Personne();

            p.DateDeNaissance = new System.DateTime(1982, 5, 6);
            p.Genre = 1;
            p.SSN = "820506-203-16";

            bool result = p.ValidateSSN();

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateSSNWithBadBirthDate()
        {
            Personne p = new Personne();

            p.DateDeNaissance = new System.DateTime(1982, 5, 7);
            p.Genre = 2;
            p.SSN = "820506-203-16";

            bool result = p.ValidateSSN();

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateSSNWithBadControlNumber()
        {
            Personne p = new Personne();

            p.DateDeNaissance = new System.DateTime(1982, 5, 6);
            p.Genre = 2;
            p.SSN = "820506-203-21";

            bool result = p.ValidateSSN();

            Assert.IsFalse(result);
        }


        [TestMethod]
        public void ValidateSSNWithCorrectValue()
        {
            Personne p = new Personne();

            p.DateDeNaissance = new System.DateTime(1982, 5, 6);
            p.Genre = 2;
            p.SSN = "820506-203-16";

            bool result = p.ValidateSSN();

            Assert.IsTrue(result);
        }
    }
}
