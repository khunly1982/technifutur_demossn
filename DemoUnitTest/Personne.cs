﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoUnitTest
{
    public class Personne
    {
        public DateTime DateDeNaissance { get; set; }

        public int Genre { get; set; } // 1 | 2

        public string SSN { get; set; }

        public bool ValidateSSN()
        {
            // vérifier le format 123456-123-12

            string toTest = SSN.Replace("-", "");
            long value;

            if(toTest.Length != 11 || !long.TryParse(toTest, out value))
            {
                return false;
            }

            // valider la date de naissance
            string year = toTest.Substring(0, 2);
            string month = toTest.Substring(2, 2);
            string day = toTest.Substring(4, 2);
            string genderValue = toTest.Substring(6, 3);
            string control = toTest.Substring(9, 2);
            long valueToControl = long.Parse(toTest.Substring(0, 9)); 

            if (DateDeNaissance.Year.ToString() != ("19" + year) && DateDeNaissance.Year.ToString() != ("20" + year))
            {
                return false;
            }

            if (DateDeNaissance.Month != int.Parse(month))
            {
                return false;
            }

            if (DateDeNaissance.Day != int.Parse(day))
            {
                return false;
            }

            if(int.Parse(genderValue) % 2 == Genre % 2)
            {
                return false;
            }

            if(97 - (valueToControl % 97) != int.Parse(control) && 97 - ((2000000000 + valueToControl) % 97) != int.Parse(control))
            {
                return false;
            }

            return true;
        }
    }
}
