﻿using System;

namespace DemoUnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entrez votre date de naissance");
            string dateDeNaissance = Console.ReadLine();
            Console.WriteLine("Entrez votre sexe");
            string genre = Console.ReadLine();
            Console.WriteLine("Entrez votre num de sécurité sociale");
            string ssn = Console.ReadLine();

            Personne p = new Personne();
            p.DateDeNaissance = DateTime.Parse(dateDeNaissance);
            p.Genre = int.Parse(genre);
            p.SSN = ssn;
        }
    }
}
